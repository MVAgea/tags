import numpy as np
import requests
import json
import re

class taggeador:
    def __init__(self,url):
        self.url = url
        self.tags_l = set([])
        self.nombres = {"base": self.taggear_base}
        
    def bajar_tags(self):
        r = requests.get(self.url)
        el_json = r.content
        el_json= json.loads(el_json)
        el_json = el_json["items"]
        tags = []
        for tag in el_json:
            tags.append(tag["name"])
        self.tags_l = set(tags)

    def taggear_base(self,nota):
        ans = []
        nota = nota.lower()
        nota = nota + " "
        for tag in self.tags_l:
            tag_lower = tag.lower()
            if (" " + tag_lower + " " in nota) | (" " + tag_lower + "." in nota):
                inicio_tag = np.array([m.start() for m in re.finditer(tag_lower,nota)])
                tmp_ans = [(init,init + len(tag)-1) for init in inicio_tag]
                tmp_ans = [tag] + tmp_ans
                ans.append(tmp_ans)
        return(ans)
    
    def taggear(self,nota,funciones_de_taggeo):
        ans = []
        for funcion in funciones_de_taggeo:
            ans = ans + self.nombres[funcion](nota)
        return ans    

### ejemplo de uso
prueba = taggeador("http://api-editoriales.clarin.com/api/contents/v3/tags/?service=agea-bi&limit=1000000&offset=0")
prueba.bajar_tags()
nota= "Mauricio Macri es el presidente. La presidente anterior fue Cristina Kirchner. Mauricio Macri perdió las PASO 2019. Disney y viajes."
taggeadores = ["base"]
prueba.taggear(nota,taggeadores)